[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://angular.io/)
[![angular](https://img.shields.io/badge/Framework-Angular-red)](https://angular.io/)

# Project 11-PhotoWall

PhotoWall is storage and sharing platform for photos, drawings and other forms of computer graphics. 
Registered users can add photos, edit info about them and sign them to specific categories.

# Demo & Presentation
- [presentation ](https://gitlab.com/matfpveb/projekti/2019-2020/11-photowall/-/raw/master/presentation/11-PhotoWall.pdf?inline=false)
- [demo-video](https://www.youtube.com/watch?v=VRRZlFFFzxE)


[![demo-video](https://gitlab.com/matfpveb/projekti/2019-2020/11-photowall/-/raw/master/presentation/thumbnail.png?inline=false)](https://www.youtube.com/watch?v=VRRZlFFFzxE) 

## Developers

- [Filip Šašić, 135/2015](https://gitlab.com/sasicf)
- [Anđela Karakaš, 86/2015](https://gitlab.com/AndjelaKarakas)
- [Miloš Krsmanović, 263/2015](https://gitlab.com/milos143)
- [Bojan Stefanović, 22/2015](https://gitlab.com/mi15022)
- [Nebojša Koturović, 139/2015](https://gitlab.com/nkoturovic)


# More info
- server read me: [https://gitlab.com/matfpveb/projekti/2019-2020/11-photowall/-/tree/master/server](https://gitlab.com/matfpveb/projekti/2019-2020/11-photowall/-/tree/master/server)
- client read me: [https://gitlab.com/matfpveb/projekti/2019-2020/11-photowall/-/tree/master/client](https://gitlab.com/matfpveb/projekti/2019-2020/11-photowall/-/tree/master/client)
