BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "refresh_tokens" (
	"user_id"	INTEGER NOT NULL,
	"refresh_token"	INTEGER NOT NULL,
	PRIMARY KEY("user_id"),
	FOREIGN KEY("user_id") REFERENCES "users"("id")
);
CREATE TABLE IF NOT EXISTS "auth_tokens" (
	"user_id"	INTEGER NOT NULL,
	"auth_token"	TEXT NOT NULL,
	PRIMARY KEY("user_id"),
	FOREIGN KEY("user_id") REFERENCES "users"("id")
);
CREATE TABLE IF NOT EXISTS "users_permissions" (
	"group_id"	INTEGER NOT NULL UNIQUE,
	"instance"	INTEGER,
	"id"	INTEGER DEFAULT 0,
	"username"	INTEGER DEFAULT 0,
	"password"	INTEGER DEFAULT 0,
	"email"	INTEGER DEFAULT 0,
	"firstname"	INTEGER DEFAULT 0,
	"lastname"	INTEGER DEFAULT 0,
	"born"	INTEGER DEFAULT 0,
	"gender"	INTEGER DEFAULT 0,
	"biography"	INTEGER,
	"join_date"	INTEGER DEFAULT 0,
	"permission_group"	INTEGER DEFAULT 0,
	PRIMARY KEY("group_id"),
	FOREIGN KEY("group_id") REFERENCES "permission_groups"("id")
);
CREATE TABLE IF NOT EXISTS "photos_permissions" (
	"group_id"	INTEGER,
	"instance"	INTEGER,
	"id"	INTEGER,
	"extension"	INTEGER,
	"title"	INTEGER,
	"category"	INTEGER,
	"description"	INTEGER,
	"uploaded_by"	INTEGER,
	"upload_time"	INTEGER,
	"is_private"	INTEGER,
	FOREIGN KEY("group_id") REFERENCES "permission_groups"("id")
);
CREATE TABLE IF NOT EXISTS "photos" (
	"id"	INTEGER NOT NULL UNIQUE,
	"extension"	TEXT NOT NULL,
	"title"	TEXT NOT NULL,
	"category"	TEXT NOT NULL,
	"description"	TEXT,
	"uploaded_by"	INTEGER,
	"upload_time"	TEXT,
	"is_private"	INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY("id"),
	FOREIGN KEY("uploaded_by") REFERENCES "users"("id")
);
CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"username"	TEXT NOT NULL UNIQUE,
	"password"	TEXT NOT NULL,
	"email"	TEXT NOT NULL UNIQUE,
	"firstname"	TEXT,
	"lastname"	TEXT,
	"born"	TEXT,
	"gender"	CHAR,
	"biography"	TEXT,
	"join_date"	TEXT,
	"permission_group"	INTEGER DEFAULT 3,
	FOREIGN KEY("permission_group") REFERENCES "permission_groups"("id")
);
CREATE TABLE IF NOT EXISTS "permission_groups" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"name"	TEXT NOT NULL UNIQUE,
	"is_user"	INTEGER
);
INSERT INTO "refresh_tokens" VALUES (2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoyfQ.wE7E69z5rvDOlBQSd4nb3mX9ZQWsAMo-49h9uk4MOsc');
INSERT INTO "refresh_tokens" VALUES (3,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjozfQ.e2OzNkccxMVImuebIR1v1vi_McrIo4pNnP-UlZpM4XA');
INSERT INTO "auth_tokens" VALUES (2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJncm91cF9pZCI6MywidXNlcl9pZCI6Mn0.jpzLfj6h834GpBSWJd_HptX1ju4roRXk1j5l3PcqDTU');
INSERT INTO "auth_tokens" VALUES (3,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJncm91cF9pZCI6MywidXNlcl9pZCI6M30.43ZsoL_sNX3AUC6y_b5YMaH-7-PXWKeplrfmzgELRvo');
INSERT INTO "users_permissions" VALUES (0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT INTO "users_permissions" VALUES (1,7,5,4,2,6,6,6,6,6,6,6,4);
INSERT INTO "users_permissions" VALUES (2,12,4,12,8,12,12,12,12,12,12,12,12);
INSERT INTO "users_permissions" VALUES (3,4,4,4,0,4,4,4,4,4,4,4,4);
INSERT INTO "users_permissions" VALUES (4,15,4,6,2,6,6,6,6,6,6,4,7);
INSERT INTO "photos_permissions" VALUES (0,0,0,0,0,0,0,0,0,0);
INSERT INTO "photos_permissions" VALUES (1,7,5,4,6,6,7,4,4,6);
INSERT INTO "photos_permissions" VALUES (2,4,4,4,4,4,4,4,4,4);
INSERT INTO "photos_permissions" VALUES (3,15,12,12,12,12,12,12,12,12);
INSERT INTO "photos_permissions" VALUES (4,15,12,12,12,12,12,12,12,14);
INSERT INTO "users" VALUES (1,'djomla96','qweqwe123','mladen@gmail.com','Mladen','Mladenovic','1994-04-20','m',NULL,'2020-06-30',3);
INSERT INTO "users" VALUES (2,'kotur','qweqwe123','necer95@gmail.com','Nebojsa','Koturovic','1995-10-10','m',NULL,'2020-07-01',3);
INSERT INTO "users" VALUES (3,'ana','qweqwe123','ana@yahoo.com','Ana','Antic','1996-06-06','f',NULL,'2020-07-02',3);
INSERT INTO "users" VALUES (24,'bojanstef996','pbg11Djd08','bojanstef996@gmail.com','Bojan','Stefanovic','1996-08-10','m',NULL,'2020-08-12',3);
INSERT INTO "permission_groups" VALUES (0,'other',NULL);
INSERT INTO "permission_groups" VALUES (1,'owner',1);
INSERT INTO "permission_groups" VALUES (2,'guest',0);
INSERT INTO "permission_groups" VALUES (3,'user',1);
INSERT INTO "permission_groups" VALUES (4,'admin',1);
COMMIT;
