import { User } from './user';
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { ApiService } from "./api.service"

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users : User[];

  constructor(
    private http : HttpClient,
    private apiService : ApiService
  ) {
    this.users = [];
   }

   addUser(user: User){
     this.users.push(user);
   }
   getUserById(id: Number){
        return this.http.get<User>(this.apiService.server + "/users/" + id, { headers : this.apiService.genHeaders() });
   }

   getUserByUsername(username: string){
    for(let user of this.users){
      if(user.username.trim() === username.trim()){
        return user;
      }
    }
    return null;
  }
}
