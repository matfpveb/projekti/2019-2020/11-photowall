export interface Photo {
    id: number;
    extension: string;
    uploaded_by: number;
    title: string;
    description: string;
    category: string;
    upload_time : string;
    is_private: boolean;
}
