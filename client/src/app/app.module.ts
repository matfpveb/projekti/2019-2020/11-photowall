import { PhotoService } from './photo.service';
import { UserService } from './user.service';
import { LoginService } from './login.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LogInComponent } from './log-in/log-in.component';
import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { EditPhotoComponent } from './edit-photo/edit-photo.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipe } from './filter.pipe';
import { ProfileComponent } from './profile/profile.component';
import { PhotoComponent } from './photo/photo.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { FilterCatPipe } from './filter-cat.pipe';
import { MatGridListModule } from '@angular/material/grid-list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



 
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LogInComponent,
    UploadPhotoComponent,
    EditPhotoComponent,
    FilterPipe,
    ProfileComponent,
    PhotoComponent,
    SignUpComponent,
    TopMenuComponent,
    FilterCatPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatGridListModule,

  ],
  providers: [LoginService, UserService, PhotoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
