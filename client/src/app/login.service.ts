import { User } from './user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loggedIn: boolean;
  user : User;
  auth_token: string;

   constructor() { 
    this.loggedIn = false;
    this.user = null;
  }

  //Set status to logged in , and set which user is logged in
  setLoggedIn(auth_token : string, usr: User){
    this.auth_token = auth_token;
    this.user = usr;
    this.loggedIn = true;
    localStorage.setItem("auth_token", auth_token);
    localStorage.setItem("user", JSON.stringify(usr));
    localStorage.setItem("loggedIn", "true");
  }

  //Set status to logged out
  setLoggedOut(){
    this.loggedIn = false;
    localStorage.setItem("loggedIn", "false");

  }

  getLoggedInStatus() {
    if (!this.loggedIn) {
      if (localStorage.getItem("loggedIn") === "true") {
         //console.log(Boolean(localStorage.getItem("loggedIn")));
         this.auth_token = localStorage.getItem("auth_token");
         this.user = JSON.parse(localStorage.getItem("user"));
         this.loggedIn = true;
      }
    }
    return this.loggedIn;
  }

  getLoggedInUser() {
    if (this.getLoggedInStatus)
      return this.user;
    else 
      return null;
  }
}