export interface User{
    id: number;
    username: string;
    password: string;
    firstname : string;
    lastname : string;
    born:string;
    biography: string;
    permission_group: number
    emial: string;
}