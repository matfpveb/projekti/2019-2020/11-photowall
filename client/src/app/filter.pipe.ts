import { Photo } from './photo';
import { Pipe, PipeTransform, IterableDiffers } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value:Photo[] ,imgSearch: string): Photo[] {

    if(value.length === null ){
      return value;
    }

    if(!imgSearch){
      return value;
    }
    imgSearch = imgSearch.toLowerCase();
    return value.filter(x => {return x.title.toLowerCase().includes(imgSearch);});

  }

}