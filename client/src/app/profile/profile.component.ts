import { UserService } from './../user.service';
import { PhotoService } from './../photo.service';
import { Photo } from './../photo';
import { LoginService } from './../login.service';
import { User } from './../user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router,NavigationEnd } from '@angular/router'
import {HttpClient} from "@angular/common/http";
import { ApiService } from "./../api.service"



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  profilePicUrl : string = '../../assets/images/profile_default.png';

  //logged in user
  loggedInUser: User;

  //user whose profile we are visitnig
  profileUser: User;

  photos: Photo[];

  _reload = true;

  msg : string = "";
  //indicates if user clicked on his own profile or not
  owner: boolean = false;
  constructor(
    private userService: UserService,
    private logInService: LoginService,
    private route: ActivatedRoute,
    private http: HttpClient,
    private apiService: ApiService,
    private photoService: PhotoService,
    public router: Router, 
    ) { 
  }

  ngOnInit(): void {

    var id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUserById(id).subscribe(resp => {
        this.profileUser = resp;

        if(!this.logInService.getLoggedInStatus()){
          this.owner = false;
          this.goToLogIn();
        } else {
          this.loggedInUser = this.logInService.user;
          this.owner = this.profileUser.id === this.loggedInUser.id;
        }

        this.photoService.getPhotosByUser(id).subscribe(resp => {
            this.photos = resp.filter(p => this.owner || !p.is_private);
        });
    });


      //Kod za refresh - preuzet sa https://stackoverflow.com/questions/48070404/angular-5-route-animations-for-navigating-to-the-same-route-but-different-para
      this.router.routeReuseStrategy.shouldReuseRoute = function(){
        return false;
        };
        this.router.events.subscribe((evt) => {
          if (evt instanceof NavigationEnd) {
              this.router.navigated = false;
              window.scrollTo(0, 0);
          }
        });
  }

  goToPhoto(id: number){
    this.router.navigate(["photo/" + id.toString()]);
  }

  goToLogIn(){
    this.router.navigate(["login"]);
  }


  onBioChange(){
    // this.userService.users[this.loggedInUser.id - 1].bio = this.loggedInUser.bio;
    var req = this.http.put(this.apiService.server + "/users/" + this.profileUser.id,
                  { biography : this.profileUser.biography }, { headers: this.apiService.genHeaders() } )
    req.subscribe(_ => {}, err => { window.alert(err["error"]["info"]); })
  }


  onFirstNameChange(){

    var req = this.http.put(this.apiService.server + "/users/" + this.profileUser.id,
                  { firstname : this.profileUser.firstname }, { headers: this.apiService.genHeaders() } )
    req.subscribe(_ => {}, err => { window.alert(err["error"]["info"]); })

  }

  onLastNameChange(){
    var req = this.http.put(this.apiService.server + "/users/" + this.profileUser.id,
                  { lastname : this.profileUser.lastname }, { headers: this.apiService.genHeaders() } )
    req.subscribe(_ => {}, err => { window.alert(err["error"]["info"]); })

  }

  showMsg(){
    this.msg = "Choose profile picture from my photos, click on the photo and select set as profile picture"
  }

  UsrLoggedOut(e){
    setTimeout(()=> this._reload = false);
    setTimeout(()=> this._reload = true);
  }


}
