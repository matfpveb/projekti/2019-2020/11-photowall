import { NgForm } from '@angular/forms';
import { LoginService } from './../login.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Router, UrlSerializer} from '@angular/router';
import {HttpClient} from "@angular/common/http";
import { ApiService } from "./../api.service"

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  msg: string = "";
  submitTouched: boolean = false;

  username_msg = ""
  email_msg = ""
  
  constructor(
    public router: Router,
    private loginService: LoginService,
    private apiService: ApiService,
    private http: HttpClient) {}

  ngOnInit(): void {
    if(this.loginService.getLoggedInStatus()){
      this.router.navigate(["home"]);
    }
  }

  onSubmit(form: NgForm) {
    this.submitTouched = true;
    this.msg = "";

    var req = this.http.post(this.apiService.server + "/users",
    {
          username: form.value.usrname,
          email: form.value.email,
          password : form.value.pass1
    }, { headers : this.apiService.genHeaders() });

    req.subscribe(succ => {
        window.alert(succ["info"]);
        this.router.navigate(["home"])
    }, 
         err => {
            for (let [key, value] of Object.entries(err["error"]["info"])) {
                this.msg += `${key}: ${value}\n`
          }
         })
  }

  gotoLogIn(){
    this.router.navigate(["login"]);
  }
}
