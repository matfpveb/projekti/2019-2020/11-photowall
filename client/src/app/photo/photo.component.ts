import { UserService } from './../user.service';
import { PhotoService } from './../photo.service';
import { Photo } from './../photo';
import { LoginService } from './../login.service';
import { User } from './../user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router'
import { ApiService } from "./../api.service"

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {

  photo : Photo;
  user: User;
  date;


  _reload = true;

  IsMyPhoto : boolean;


  dlCount : number = 16;
  constructor(
    private userService: UserService,
    private logInService: LoginService,
    private route: ActivatedRoute,
    private photoService: PhotoService,
    private apiService: ApiService,
    public router: Router,
    ) {
  }
  ngOnInit(): void {
    var id = + this.route.snapshot.paramMap.get('id');
    this.photoService.getPhotoById(id).subscribe(resp => { 
        this.photo = resp 
        this.userService.getUserById(this.photo.uploaded_by).subscribe(resp => this.user = resp);

        if(this.logInService.getLoggedInStatus()){

          this.IsMyPhoto = this.logInService.getLoggedInUser().id == this.photo.uploaded_by; 
        }else{
          this.IsMyPhoto = false;
        }

        this.date = new Date(this.photo.upload_time);
        this.date = this.date.toDateString();

    });



  }

  UsrLoggedOut(e){
    setTimeout(()=> this._reload = false);
    setTimeout(()=> this._reload = true);
  }

  goToEdit(id:number){
    this.router.navigate(["editPhoto/" + id.toString()]);

  }

  goToUserProfile(id:number){
    this.router.navigate(["profile/" + id.toString()]);
  }

}
