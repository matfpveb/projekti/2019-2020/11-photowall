import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import {HttpHeaders} from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  server : string = "/api";
  url : string = "http://kotur.xyz";
  constructor(private loginService : LoginService) { }

  genHeaders() {
      let headers = new HttpHeaders();
      if (this.loginService.getLoggedInStatus()) {
          headers = headers.append('Authorization', 'Bearer ' + this.loginService.auth_token);
      }
      return headers;
  }
}
