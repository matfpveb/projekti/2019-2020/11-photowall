import { Photo } from './photo';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from "@angular/common/http";
import { ApiService } from "./api.service"

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  
  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) {}
  

  getPhotoById(id: Number) {
    return this.http.get<Photo>(this.apiService.server + "/photos/" + id, { headers : this.apiService.genHeaders() });
  }

  getPhotosByUser(id: number){
    return this.http.get<Photo[]>(this.apiService.server + "/photos_by/" + id, { headers : this.apiService.genHeaders() });
  }

  getPhotos(){
    return this.http.get<Photo[]>(this.apiService.server + "/photos", { headers : this.apiService.genHeaders() });
  }

}
