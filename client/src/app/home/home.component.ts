import { CATEGORIES } from './../categories';
import { UserService } from './../user.service';
import { LoginService } from './../login.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { PhotoService } from './../photo.service';
import { Photo } from './../photo';
import { ApiService } from "./../api.service"

interface dim {
  h: number;
  w: number;
}



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('pic1', {static:false}) pic1: ElementRef;
  photos : Photo[] = [];
  categories = CATEGORIES;

  imgSearch : string = '';

  category : string = '';

  dims : dim[] = [];
  dim : dim;

  ok : boolean= false;

  map1 : Map<number,dim> = new Map<number,dim>();

  _reload = true;
  constructor(
    private photoService: PhotoService,
    private userService: UserService,
    private loginService : LoginService,
    private apiService : ApiService,
    public router: Router
    ){}

  goToPhoto(id: number){
      this.router.navigate(["photo/" + id.toString()]);
    }
  searchCat(cat: string){
    if(this.category === cat){
      this.category = "";
    }else{
      this.category = cat;
    }
  }

  public list = [];


  ngOnInit(): void {

    
    this.photoService.getPhotos().subscribe(ps => { 
      this.photos = ps.filter(p => !p.is_private);
      this.shuffle();  


    }); 
    
    

    this.list = [
      { rows: 2, cols: 2, data: 1 },
      { rows: 1, cols: 1, data: 2 },
      { rows: 2, cols: 1, data: 3 },
      { rows: 1, cols: 1, data: 4 },

      { rows: 1, cols: 1, data: 5 },
      { rows: 2, cols: 2, data: 6 },
      { rows: 1, cols: 1, data: 7 },
      { rows: 1, cols: 1, data: 8 },
      { rows: 1, cols: 1, data: 9 },

      { rows: 1, cols: 1, data: 10 },
      { rows: 1, cols: 1, data: 11 },
      { rows: 2, cols: 2, data: 12 },
      { rows: 1, cols: 1, data: 13 },
      { rows: 1, cols: 1, data: 14 },
      
    ];
  }

  loadImg(img,id){
    this.dim = {h:img.naturalHeight,w:img.naturalWidth};
    var ratio = this.dim.h/this.dim.w;
    this.dim.w = 4;
    this.dim.h = Math.round(ratio * this.dim.w);

    if(this.dim.h > 12){
      this.dim.h = 12;
    }
    if(this.dim.h === 0){
      this.dim.w = 1;
    }

    this.dims.push(this.dim);

    if(this.dims.length === this.photos.length){
      this.ok = true;
    }

    this.map1.set(id,this.dim);
  }

  //Knuth shuffle algorithm
  shuffle(){
    for(let i = this.photos.length - 1; i > 0;i--){
      var j = Math.floor(Math.random() * i);
      var temp = this.photos[i];
      this.photos[i] = this.photos[j];
      this.photos[j] = temp;

    }
  }

  UsrLoggedOut(e){
    setTimeout(()=> this._reload = false);
    setTimeout(()=> this._reload = true);
  }

}
