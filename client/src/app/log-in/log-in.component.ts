import { UserService } from './../user.service';
import { User } from './../user';
import { LoginService } from './../login.service';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { NgForm, FormsModule } from '@angular/forms';
import {HttpClient} from "@angular/common/http"
import { ApiService } from './../api.service';
import * as jwt_decode from "jwt-decode"

interface LoginResponse {
  auth_token : string;
  refresh_token : string;
}

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  loginTouched : boolean = false;
  msg: string = ""

  constructor(
    public router: Router, 
    private logInService: LoginService,
    private apiService: ApiService,
    private userService: UserService,
    private http: HttpClient
    ) { }

  ngOnInit(): void {
    if(this.logInService.getLoggedInStatus()){
      this.router.navigate(["home"]);
    }
  }

  onSubmit(form: NgForm) {
    this.loginTouched = true;
    var user : User;

    if(form.valid){

      var loginReq = this.http.post<LoginResponse>(this.apiService.server + "/login",
      {
            username: form.value.name,
            password : form.value.pass
      }, { headers : this.apiService.genHeaders() });

      loginReq.subscribe(data => { 
        var decoded_token = jwt_decode(data.auth_token);
        this.http.get<User>(this.apiService.server + "/users/" + decoded_token["user_id"], { headers : this.apiService.genHeaders() } )
            .subscribe(user => {
                 this.logInService.setLoggedIn(data.auth_token, user);
                 this.router.navigate(["home"]);               
                 window.alert("Successfully logged in");
            }, err => { this.msg = err["error"]["info"] })
      }, err => { this.msg = err["error"]["info"] });
    }
  }

  goToSignUp(){
    this.router.navigate(["signUp"]);
  }

}
