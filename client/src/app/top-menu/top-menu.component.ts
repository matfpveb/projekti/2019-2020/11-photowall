import { Router } from '@angular/router';
import {EventEmitter} from '@angular/core'
//to delete
import { UserService } from './../user.service';
import { User } from './../user';
import { LoginService } from './../login.service';
import { Component, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {

  loggedIn : boolean = false;

  @Output() loggedOut = new EventEmitter<boolean>();
  user: User;

  constructor(private loginService: LoginService, private UserService: UserService,
    public router: Router) {
  }

  ngOnInit(): void {
    this.loggedIn = this.loginService.getLoggedInStatus();
    if(this.loggedIn){
      this.user = this.loginService.user;
    }
  }

  logOut(){
    this.loginService.setLoggedOut();
    this.loggedOut.emit(true);
    this.router.navigate(["home"]);
  }
}
