import { Pipe, PipeTransform } from '@angular/core';
import { Photo } from './photo';

@Pipe({
  name: 'filterCat'
})
export class FilterCatPipe implements PipeTransform {

  transform(value:Photo[] ,imgSearch: string): Photo[] {

    if(value.length === null ){
      return value;
    }

    if(!imgSearch){
      return value;
    }
    
    
    return value.filter(x => {return x.category === imgSearch;});

  }


}
