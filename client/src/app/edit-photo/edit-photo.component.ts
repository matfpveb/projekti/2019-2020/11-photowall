import { CATEGORIES } from './../categories';
import { UserService } from './../user.service';
import { PhotoService } from './../photo.service';
import { Photo } from './../photo';
import { LoginService } from './../login.service';
import { User } from './../user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router'
import { ThrowStmt } from '@angular/compiler';
import { ApiService } from "./../api.service"
import {HttpClient} from "@angular/common/http";
import { NgForm, FormsModule } from '@angular/forms';
import { formatCurrency } from '@angular/common';


@Component({
  selector: 'app-edit-photo',
  templateUrl: './edit-photo.component.html',
  styleUrls: ['./edit-photo.component.css']
})
export class EditPhotoComponent implements OnInit {

  _reload = true;

  categories;

  photo : Photo;

  submitTouched: boolean = false;

  constructor(
    private userService: UserService,
    private logInService: LoginService,
    private apiService: ApiService,
    private http: HttpClient,
    private route: ActivatedRoute,
    private photoService: PhotoService,
    public router: Router, 
  ) { }

  ngOnInit(): void { 
    if(!this.logInService.getLoggedInStatus()){
			this.router.navigate(["home"]);
		}
    var id = +this.route.snapshot.paramMap.get('id');
    this.photoService.getPhotoById(id).subscribe(resp => {
        this.photo = resp
    });

    this.categories = CATEGORIES;
  }

  UsrLoggedOut(e){
    setTimeout(()=> this._reload = false);
    setTimeout(()=> this._reload = true);
  }


  onRemovePhoto() {
    var req = this.http.delete(this.apiService.server + "/photos/" + this.photo.id, { headers : this.apiService.genHeaders() });
    req.subscribe(succ => window.alert(succ["info"]), err => window.alert(err["error"]["info"]));
    this.router.navigate(["home"]);
  }

  onSubmit(form: NgForm) {
    this.submitTouched = true;
    if(form.valid){
      var req = this.http.put(this.apiService.server + "/photos/" + this.photo.id, this.photo, { headers : this.apiService.genHeaders() });
      req.subscribe(succ => window.alert(succ["info"]), err => window.alert(err["error"]["info"]));
      this.router.navigate(["photo/" + this.photo.id]);
    }
  }

}
