import { CATEGORIES } from './../categories';
import { UserService } from './../user.service';
import { PhotoService } from './../photo.service';
import { Photo } from './../photo';
import { LoginService } from './../login.service';
import { User } from './../user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router'
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { ApiService } from "./../api.service"
import { NgForm, FormsModule } from '@angular/forms';



@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.css']
})
export class UploadPhotoComponent implements OnInit {
	url;
    fileName : string;
  	_reload = true;
	msg = "";
	category;
	categories;
	photoName: string;
	isPrivate: boolean = false;
	descr: string;
	file;
	submitTouched: boolean = false;
	submited : boolean;

	onChangeEvent(event) {

		this.file = event.target.files[0];

		if(this.file !== undefined){
			var fileType = this.file.type;
		
			if (fileType.match(/image\/*/) == null) {
				this.msg = "Please select an image";
				return;
			}
			
			var reader = new FileReader();
			reader.readAsDataURL(this.file);
			this.fileName = this.file.name;
			
			reader.onload = () => {
				this.msg = "";
				this.url = reader.result; 
			}
		}else{
			this.url = "";
		}

	}

  	constructor(
		private userService: UserService,
		private apiService: ApiService,
		private photoService: PhotoService,
		private loginService: LoginService,
		private http: HttpClient,
		public router: Router) {}

	ngOnInit(): void {
		this.category = "Other";
		if(!this.loginService.getLoggedInStatus()){
			this.router.navigate(["home"]);
		}
		this.categories = CATEGORIES;
  	}

	OnReset(){
		this.url = "";
	}

	onSubmit(form:NgForm){
		this.submitTouched = true;
		if(form.valid && !this.submited){
			this.submited = true;
			var photo = { 
				uploaded_by : this.loginService.user.id,
				title : this.photoName,
				description : this.descr,
				category : this.category,
				is_private : this.isPrivate
			};

			const formData = new FormData();
			formData.append("json", JSON.stringify(photo));
			formData.append("file", this.file, this.fileName);

			var req = this.http.post(this.apiService.server + "/photos", formData, { headers: this.apiService.genHeaders() });

			req.subscribe(resp => {
					this.router.navigate(["photo/" + resp["info"]]);
				}, err => { window.alert(err["error"]["info"]) }
			);
		}
	}

  UsrLoggedOut(e){
    	setTimeout(()=> this._reload = false);
    	setTimeout(()=> this._reload = true);
  }

}
