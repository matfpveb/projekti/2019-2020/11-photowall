import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { ProfileComponent } from './profile/profile.component';
import { PhotoComponent } from './photo/photo.component';
import { LogInComponent } from './log-in/log-in.component';
import { HomeComponent } from './home/home.component';
import { EditPhotoComponent } from './edit-photo/edit-photo.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LogInComponent},
  {path: 'profile/:id', component: ProfileComponent},
  {path: 'uploadPhoto', component: UploadPhotoComponent},
  {path: 'photo/:id', component: PhotoComponent},
  {path: 'editPhoto/:id', component: EditPhotoComponent},
  {path: 'signUp', component: SignUpComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
